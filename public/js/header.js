var href = document.location.href;
var htmlFile = href.substr(href.lastIndexOf('/') + 1);

var html = '  <div class="header-top">' +
    '    <div class="header-top-search">' +
    '        <div class="row">' +
    '          <div class="col-md-3 col-sm-6">' +
    '            <div class="header-logo" style="margin-left:20px">' +
    '              <a href="index.html"><img src="img/header-logo.png" class="img-responsive" alt="logo" /></a>' +
    '            </div>' +
    '          </div>' +
    '          <div class="col-md-5"></div>' +
    '          <!-- <div class="col-sm-4"></div> -->' +
    '          <div class="col-md-4 col-sm-6">' +
    '            <div class="search-holder">' +
    '              <!--<input type="text" class="form-control" placeholder="Search">-->' +
    '              <ul id="header-car-container" class="list-inline">' +
    '                <li style="float: right;"><a href="signup.html"><img src="img/others/uber.png" class="img-responsive" alt="car" /></a></li>' +
    '                <li style="float: right;"><a href="signup.html"><img src="img/others/lyft.png" class="img-responsive" alt="car" /></a></li>' +
    '                <li style="float: right;"><a href="signup.html"><img src="img/others/taxi.png" class="img-responsive" alt="car" /></a></li>' +
    '              </ul>' +
    '            </div>' +
    '          </div>' +
    '        </div>' +
    '    </div>' +
    '    <div class="mainmenu">' +
    '      <div class="container">' +
    '        <nav class="navbar top-navbar">' +
    '          <div class="navbar-header">' +
    '            <button data-target="#bs-example-navbar-collapse-1" data-toggle="collapse" class="navbar-toggle" type="button">' +
    '              <span class="sr-only">Toggle navigation</span>' +
    '              <span class="icon-bar"></span>' +
    '              <span class="icon-bar"></span>' +
    '              <span class="icon-bar"></span>' +
    '            </button>' +
    '          </div>' +
    '          <div id="bs-example-navbar-collapse-1" class="collapse navbar-collapse">' +
    '            <ul class="nav navbar-nav">' +
    '              <li><a href="index.html">Home</a></li>' +
    '              <li><a href="about.html">ABOUT</a></li>'
    // +'              <li><a href="signup.html">SIGN UP</a></li>'
    // +'              <li><a href="signin.html">SIGN IN</a></li>'
    +
    '              <li><a href="support.html">SUPPORT</a></li>' +
    '              <li><a href="contact.html">CONTACT</a></li>' +
    '            </ul>' +
    '          </div>' +
    '        </nav>' +
    '      </div>' +
    '    </div>' +
    '  </div>' +
    '  <div class="mobile-header">' +
    '    <div class="container">' +
    '      <div class="mobile-head-contents">' +
    '        <div class="row">' +
    '          <div class="col-xs-2">' +
    '            <div class="mobile-menu">' +
    '              <a class="menu-icon" href="#"><i class="fa fa-bars"></i></a>' +
    '            </div>' +
    '          </div>' +
    '          <div class="col-xs-8">' +
    '            <div class="mobile-logo text-center">' +
    '              <a href="index.html"><img src="img/header-logo.png" class="img-responsive" alt="logo" /></a>' +
    '            </div>' +
    '          </div>' +
    '          <div class="col-xs-2">' +
    '            <div class="mobile-search">' +
    '              <a class="search-icon" href="support.html"><i class="fa fa-search"></i></a>' +
    '            </div>' +
    '          </div>' +
    '        </div>' +
    '      </div>' +
    '    </div>' +
    '  </div>' +
    '  <div class="search-panel">' +
    '    <!--<input type="text" class="form-control" placeholder="Search">-->' +
    '    <ul class="list-inline">' +
    '      <li><a href="about.html"><img src="img/others/uber.png" class="img-responsive" alt="car" /></a></li>' +
    '      <li><a href="about.html"><img src="img/others/lyft.png" class="img-responsive" alt="car" /></a></li>' +
    '      <li><a href="about.html"><img src="img/others/taxi.png" class="img-responsive" alt="car" /></a></li>' +
    '    </ul>' +
    '  </div>' +
    '  <div class="mobile-drop-down">' +
    '    <ul class="list-unstyled">' +
    '      <li><a href="index.html">Home</a></li>' +
    '      <li><a href="about.html">ABOUT</a></li>'
    // +'      <li><a href="signup.html">SIGN UP</a></li>'
    // +'      <li><a href="signin.html">SIGN IN</a></li>'
    +
    '      <li><a href="support.html">SUPPORT</a></li>' +
    '      <li><a href="contact.html">CONTACT</a></li>' +
    '    </ul>' +
    '  </div>';

if(htmlFile == 'index.html' || htmlFile == ''){
  html += '<div class="header-bottom">' +
    '    <img src="img/banner/banner.jpg" class="img-responsive" alt="banner" />' +
    '    <form id="registration-form" class="sign-up-form" data-toggle="validator">' +
    '        <div id="reg_id" class="sign-up-heading">' +
    '            <h3>Register Now</h3>' +
    '        </div>' +
    '        <div class="form-contents">' +
    '            <div class="form-group">' +
    '                <input id="first-name-input" type="text" class="form-control" placeholder="First Name" required>' +
    '            </div>' +
    '            <div class="form-group">' +
    '                <input id="last-name-input" type="text" class="form-control" placeholder="Last Name" required>' +
    '            </div>' +
    '            <div class="form-group">' +
    '                <input id="email-input" type="email" class="form-control" placeholder="Email" required>' +
    '            </div>' +
    '            <div class="form-group">' +
    '                <input id="phone-input" type="text" class="form-control" placeholder="Phone" required>' +
    '            </div>' +
    '            <div class="form-group">' +
    '                <input id="password-input" type="text" class="form-control" placeholder="Create Password" required>' +
    '            </div>' +
    '            <div class="form-group">' +
    '                <input id="city-input" type="text" class="form-control" placeholder="City" required>' +
    '            </div>' +
    '            <div class="form-group">' +
    '                <input id="invite-code-input" type="text" class="form-control" placeholder="Invite Code (optional)">' +
    '            </div>' +
    '        </div>' +
    '        <div class="form-btn">' +
    '            <a type="submit" id="registration-button" class="btn form-btn">Register Now</a>' +
    '        </div>' +
    '    </form>' +
    '</div>';
}

    html += '</div';

document.getElementById('header-target').innerHTML = html;

//adds the listener for form submission
if(htmlFile === 'index.html' || !htmlFile){
    console.log('index file');
    $('#registration-form').validator();
    $('#registration-button').on('click', (e) => {
        console.log('click')
        $('#registration-form').submit();
    });
    $('#registration-form').on('submit', function (e) {
        if (e.isDefaultPrevented()) {
            console.log('not valid form');
            $('#registrationErrorModal').modal();
            $('#registrationErrorModal').modal('show');
        } else {
            e.preventDefault();
            // $.ajax({
            //         url: "https://ridetagregistrationapi.herokuapp.com/api/authenticate/user",
            //         data: {
            //             'id_input' : $('#updateUser').data('id'),
            //             'first_name_input' : $('#first_name_update').val(),
            //             'last_name_input': $('#last_name_update').val(),
            //             'email_input': $('#email_update').val(),
            //             'phone_input': $('#phone_update').val(),
            //             'password_input': $('#password_update').val(),
            //             'city_input': $('#city_update').val(),
            //             'invite_code_input': $('#invite_code_update').val()
            //         },
            //         type: "POST",
            //         dataType: "json"
            //     })
            //     .done(function(data) {
            //         if (data.status == true) {
            //             alert(data.message);
            //             // $.ajax({
            //             //     url: "http://localhost:8080/index",
            //             //     type: "GET"
            //             // });
            //             window.location = data.redirect; //redirects it to login if credentials are right
            //         } else {
            //             alert("Could not update user!");
            //             console.log(data.error);
            //         }
            //     });

                $.ajax({
                    url: "https://ridetagregistrationapi.herokuapp.com/api/authenticate",
                    type: "GET",
                    dataType: "jsonp"
                })
                .done(function(data) {
                    if (data.success) {
                        $.ajax({
                            url: "https://ridetagregistrationapi.herokuapp.com/api/user",
                            data: {
                                'first_name_input' : $('#first-name-input').val(),
                                'last_name_input': $('#last-name-input').val(),
                                'email_input': $('#email-input').val(),
                                'phone_input': $('#phone-input').val(),
                                'password_input': $('#password-input').val(),
                                'city_input': $('#city-input').val(),
                                'invite_code_input': $('#invite-code-input').val()
                            },
                            type: "POST",
                            dataType: "jsonp"
                        })
                        .done(function(data) {
                            if (data.success) {
                                $('#registrationSuccessModal').modal();
                                $('#registrationSuccessModal').modal('show');
                            } else {
                                console.log("Could not register user, post");
                                console.log(data);
                            }
                        });
                    } else {
                        console.log("Could not register user, authentication!");
                        console.log(data);
                    }
                });
        }
    });
}
