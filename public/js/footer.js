var html = '<div class="container">' +
    '  <div class="footer-top">' +
    '    <div class="row">' +
    '      <div class="col-xs-4 col-sm-4 col-lg-4">' +
    '        <div class="foot-list">' +
    '          <ul class="list-unstyled">' +
    '            <li><a class="popup" href="#" data-toggle="tooltip" data-placement="right" title="Coming Soon!"><i class="fa fa-map-marker"></i>Enter Your Location</a></li>' +
    '            <li><a href="#"><i class="fa fa-globe"></i>ENGLISH</a></li>' +
    '            <li><a href="#"><i class="fa fa-life-saver"></i>Help</a></li>' +
    '          </ul>' +
    '        </div>' +
    '        <div class="foot-social">' +
    '          <ul class="list-inline">' +
    '            <li><a href="https://www.facebook.com/GoRideTag"><i class="fa fa-facebook"></i></a></li>' +
    '            <li><a href="https://www.twitter.com/ridetag"><i class="fa fa-twitter"></i></a></li>' +
    '            <li><a href="https://www.linkedin.com/company-beta/17988723/"><i class="fa fa-linkedin"></i></a></li>' +
    '            <li><a href="https://www.instagram.com/ridetag/"><i class="fa fa-instagram"></i></a></li>' +
    '          </ul>' +
    '        </div>' +
    '      </div> <!--end of first row-->' +
    '      <div class="col-xs-4 col-sm-2 col-lg-2">' +
    '        <div class="foot-list">' +
    '          <ul class="list-unstyled">' +
    '            <li><a href="index.html#reg_id">Ride</a></li>' +
    '            <li><a data-toggle="modal" data-target="#driveModal">Drive</a></li>' +
    '            <li><a href="#">Cities</a></li>' +
    '            <li><a href="#">Fare Estimate</a></li>' +
    '            <li><a href="#">How it Works</a></li>' +
    '          </ul>' +
    '        </div>' +
    '      </div> <!--end of ride, drive, cities, fare estimate column-->' +
    '      <div class="col-xs-4 col-sm-2 col-lg-2">' +
    '        <div class="foot-list">' +
    '          <ul class="list-unstyled">' +
    '            <li><a href="#">Safety</a></li>' +
    '            <li><a href="#">Careers</a></li>' +
    '            <li><a href="#">Helping Cities</a></li>' +
    '            <li><a href="#">Our Story</a></li>' +
    '            <li><a href="#">Newsroom</a></li>' +
    '          </ul>' +
    '        </div>' +
    '      </div> <!--end of saftey, careers, helping cities, column-->' +
    '      <div class="col-xs-12 col-sm-4 col-lg-4">' +
    '        <div class="foot-appstore">' +
    '          <ul class="list-inline">' +
    '            <li><a href="#"><img src="img/others/apple-icon.png" class="img-responsive" alt="appstore" /></a></li>' +
    '            <li><a href="#"><img src="img/others/google-icon.png" class="img-responsive" alt="appstore" /></a></li>' +
    '          </ul>' +
    '        </div>' +
    '      </div> <!--end of app store column-->' +
    '    </div>' +
    '  </div>' +
    '' +
    '  <div class="footer-bottom text-center">' +
    '    <p>Copyright &copy; 2017. All Rights Reserved</p>' +
    '  </div>' +
    '</div>' +
    '<!--below here is modal pop up that links to ride options-->' +
    '<div class="modal fade" id="driveModal" tabindex="-1" role="dialog" aria-labelledby="driveModal" aria-hidden="true">' +
    '  <div class="modal-dialog">' +
    '    <div class="modal-content">' +
    '      <div class="modal-header">' +
    '        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
    '        <center><h4 class="modal-title" id="myModalLabel">Sign Up to Drive</h4></center>' +
    '      </div>' +
    '      <div class="modal-body">' +
    '        <a href="https://www.uber.com/a/carousel-vs-1-pp/?city_name=los-angeles&utm_source=AdWords_Brand&utm_campaign=search-google-brand_1_12_us-losangeles_d_txt_acq_cpc_en-us_uber_kwd-169801042_183157357638_22880494120_e_c_track-apr25playstore_restructure&cid=271789120&adg_id=22880494120&fi_id=&match=e&net=g&dev=c&dev_m=&cre=183157357638&kwid=kwd-169801042&kw=uber&placement=&tar=&gclid=Cj0KEQjw5YfHBRDzjNnioYq3_swBEiQArj4pdESKyS3kS7BMIkojCe2zukrnDmtS3lH84FxN2H_1bnkaAqvN8P8HAQ&gclsrc=aw.ds&dclid=COTd5YrYidMCFZB-fgodh8MHPQ" class="btn btn-block btn-social btn-lg btn-uber" style="color:#fff;background-color:#444;border-color:rgba(0,0,0,0.2)"><img class="customIcon" src="./img/icons/uber-logo.png"><i class="fa fa-uber"></i> Sign Up With Uber</a>' +
    '        <a href="https://www.lyft.com/drive-with-lyft?loc_physical_ms=9060568&utm_campaign=Driver_RE_US_Search_Brand_Lyft_Exact&devicemodel=&adgroup=lyft&adposition=1t1&route_key=dax-bt&var3=brand_jan17_c&utm_term=lyft&gclid=Cj0KEQjw5YfHBRDzjNnioYq3_swBEiQArj4pdE1BQGLb0U6EMd3M7MCzco0Cokviuj1QIVVtiJU37ckaAqPG8P8HAQ&targetid=kwd-158399963&campaign_id=668992537&utm_source=google&network=g&ad_id=183025307695&device=c&k_clickid=4bca57a3-42e7-4c3e-8981-8aa3855a86cc&matchtype=e&ref=DRIVER250BONUS&adname=brand_jan17_c&adgroup_id=32053971777&loc_interest_ms=" class="btn btn-block btn-social btn-lg btn-lyft" style="color:#fff;background-color:#ff0084;border-color:rgba(0,0,0,0.2)"><img class="customIcon" src="./img/icons/lyft-logo.png"><i class="fa fa-lyft"></i> Sign Up With Lyft</a>' +
    '        <a href="https://www.waze.com/carpool/index.html" class="btn btn-block btn-social btn-lg btn-waze" style="color:#fff;background-color:#517fa4;border-color:rgba(0,0,0,0.2)"><img class="customIcon" src="./img/icons/waze-logo.png"><i class="fa fa-waze"></i> Sign Up with Waze</a>' +
    '      </div>' +
    '    </div>' +
    '  </div>' +
    '</div>';

document.getElementById('footer-target').innerHTML = html;

//Activate tooltip for location to display "COMING SOON"
$('[data-toggle="tooltip"]').tooltip();
