$(function() {
	
	 // tabbed content
    // http://www.entheosweb.com/tutorials/css/tabs.asp
    $(".tab_content").hide();
    $(".tab_content:first").show();

  /* if in tab mode */
    $("ul.tabs li").click(function() {
		
      $(".tab_content").hide();
      var activeTab = $(this).attr("rel"); 
      $("#"+activeTab).fadeIn();		
		
      $("ul.tabs li").removeClass("active");
      $(this).addClass("active");

	  $(".tab_drawer_heading").removeClass("d_active");
	  $(".tab_drawer_heading[rel^='"+activeTab+"']").addClass("d_active");
	  
    });
	/* if in drawer mode */
	$(".tab_drawer_heading").click(function() {
      
      $(".tab_content").hide();
      var d_activeTab = $(this).attr("rel"); 
      $("#"+d_activeTab).fadeIn();
	  
	  $(".tab_drawer_heading").removeClass("d_active");
      $(this).addClass("d_active");
	  
	  $("ul.tabs li").removeClass("active");
	  $("ul.tabs li[rel^='"+d_activeTab+"']").addClass("active");
    });
	
	
	/* Extra class "tab_last" 
	   to add border to right side
	   of last tab */
	$('ul.tabs li').last().addClass("tab_last");
	
	
	
	$(".menu-icon").click(function(){
		$(".mobile-drop-down").slideToggle();
	});
	
	$(".search-icon").click(function(){
		$(".search-panel").slideToggle();
	});
	
	$('.owl-carousel').owlCarousel({
		loop:true,
		lazyLoad:true,
		margin:100,
		autoplay:true,
		autoplayTimeout:5000,
		touchDrag  : true,
		mouseDrag  : true,
		dots: false,
		responsiveClass:true,
		responsive:{
			1366:{
				items:2,
				nav:true
			},
			1024:{
				items:2,
				nav:true
			},
			640:{
				items:1,
				nav:true
			},
			0:{
				items:1,
				nav:true
			}
		}
	})
});