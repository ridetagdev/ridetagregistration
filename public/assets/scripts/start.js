var uuid = function() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = Math.random() * 16 | 0,
      v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}

var link = function(event) {
  event.preventDefault();
  var apigClient = apigClientFactory.newClient();
  const user = {
    user_id: uuid(),
    telephone: $('#telephone').val(),
    pin: $('#pin').val()
  }
  apigClient.usersPost({}, user).then(function(result) {
    const clientId = 'eO7lSdczQOi3pPeTjVzuMPzggq5ylTdz';
    const uber = 'https://login.uber.com/oauth/v2/authorize?client_id=' + clientId + '&response_type=code&state=' + user.telephone;

    location.href = uber;
  }).catch(function(result) {
    var error = result.data.message;
    if (result.data.code === 'ConditionalCheckFailedException') {
        error = 'That number is already registered.';
    }
    alert(error);
  });
}

var linked = function() {
  var queries = {};
  $.each(document.location.search.substr(1).split('&'), function(c, q) {
    var i = q.split('=');
    queries[i[0].toString()] = i[1].toString();
  });

  var apigClient = apigClientFactory.newClient();

  var params = {
    telephone: queries.state
  }
  const user = {
    code: queries.code
  }

  apigClient.userTelephonePut(params, user).then(function(result) {
    $('.modal').removeClass('is-active');
  }).catch(function(result) {
    event.preventDefault();
    alert(result);
  });


}
