const poolData = {
  UserPoolId: 'us-west-2_93DpF1NT7',
  ClientId: '5rgv5n5ocj5okpalkrdkjviig5'
};

var uuid = function() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = Math.random() * 16 | 0,
      v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}

var signUp = function(event) {
  event.preventDefault();
  var poolData = {
    UserPoolId: 'us-west-2_93DpF1NT7',
    ClientId: '5rgv5n5ocj5okpalkrdkjviig5'
  };
  var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(poolData);

  var attributeList = [];

  var dataPhoneNumber = {
    Name: 'phone_number',
    Value: '+1' + $('#phone').val()
  };

  var dataUberAccount = {
    Name: 'custom:UberAccount',
    Value: $('#uber').val()
  };

  var dataPIN = {
    Name: 'custom:PIN',
    Value: $('#pin').val()
  };

  var attributePhoneNumber = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserAttribute(dataPhoneNumber);
  var attributeUberAccount = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserAttribute(dataUberAccount);
  var attributePIN = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserAttribute(dataPIN);

  attributeList.push(attributePhoneNumber);
  attributeList.push(attributeUberAccount);
  attributeList.push(attributePIN);

  var userId = dataUberAccount.Value;
  var password = $('#password').val();

  userPool.signUp(userId, password, attributeList, null, function(err, result) {
    if (err) {
      alert(err);
      return;
    }
    cognitoUser = result.user;
    console.log('user name is ' + cognitoUser.getUsername());
  });
}

var confirm = function(event) {
  event.preventDefault();

  var confirmUber = $('#confirmUber').val()

  var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(poolData);
  var userData = {
    Username: confirmUber,
    Pool: userPool
  };

  var cognitoUser = new AWSCognito.CognitoIdentityServiceProvider.CognitoUser(userData);

  var confirmationCode = $('#confirmationCode').val()

  cognitoUser.confirmRegistration(confirmationCode, true, function(err, result) {
    if (err) {
      alert(err);
      return;
    }
    console.log('call result: ' + result);
  });
}

var resend = function(event) {
  event.preventDefault();

  var resendUber = $('#resendUber').val()

  var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(poolData);
  var userData = {
    Username: resendUber,
    Pool: userPool
  };

  var cognitoUser = new AWSCognito.CognitoIdentityServiceProvider.CognitoUser(userData);

  cognitoUser.resendConfirmationCode(function(err, result) {
    if (err) {
      alert(err);
      return;
    }
    console.log('call result: ' + result);
  });
}
