# RideTag Registration Site #

This is the one that is deployed to www.ridetag.net and has the most up to date code. This is the repo for
the pre-production (signup) version of the website and is used to collect user emails. This is not the full
production site but will be used to gauge user interest.

Front-End Techstack: HTML, CSS, Bootstrap, Jquery, VanillaJS


# Documentation

## Starting Development environment
1. clone the repository
2. npm install
3. gulp webserver (this will automatically deploy to a local server on your machine w/ live reloading!)

## Header and Footer Modifications
All header and footer modifications should be done in js/footer.js or js/header.js files.
They are dynamically loaded onto the page so that all modifications can be accounted for without modifying
every single html file.

header.js file has a special check to see if the current page is index.html. If it is, it will also display
the registration form so that people who are interested can input their information. Validation is done on
the front end before POST request is made to our Registration API 2.0.

## Styling
All custom css can be found in css/style.css. Will be modularized into their own respective css files.


# Deployment
Deployment is automatic. Any changes to the origin master branch wil be deployed to www.ridetag.net domain.
There is a BitBucket pipeline that streamlines the gulp-awspublish node module to deploy our static Front-End
code over to the AWS bucket.
