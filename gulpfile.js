var gulp = require('gulp');
var awspublish = require('gulp-awspublish');
var webserver = require('gulp-webserver');

gulp.task('publish', function() {

  var publisher = awspublish.create({
    region: 'us-west-1',
    params: {
      Bucket: 'ridetag'
    },
    accessKeyId: process.env.AWS_KEY,
    secretAccessKey: process.env.AWS_SECRET
  });

  return gulp.src('public/**')
    .pipe(publisher.publish())
    .pipe(publisher.sync())
    .pipe(awspublish.reporter());
});


gulp.task('webserver', function() {
  gulp.src('public')
    .pipe(webserver({
      livereload: true,
      directoryListing: false,
      open: true
    }));
});
